# Description

Scripts to perform the alignment tasks involved in the study: 

*"Simulation-based comprehensive benchmarking of RNA-seq aligners"*, G. Baruzzo, K. E. Hayer, E. J. Kim, B. Di Camillo, G. FitzGerald and G. R. Grant, Nature Methods, Published online 12 December 2016, doi:10.1038/nmeth.4106

Study website: http://bioinf.itmat.upenn.edu/BEERS/bp1/index.php

The last version of these script is available at: https://bitbucket.org/baruz/aligner-benchmark

---

# Directory Contents
#### job
The folder `job` contains several subfolders:

  - `contextmap2`: contains the scripts to create the reference index and align the data using Contextmap2
  - `crac`: contains the scripts to create the reference index and align the data using CRAC
  - `gsnap`: contains the scripts to create the reference index and align the data using GSNAP
  - `hisat`: contains the scripts to create the reference index and align the data using HISAT
  - `hisat2`: contains the scripts to create the reference index and align the data using HISAT2
  - `mapsplice2`: contains the scripts to create the reference index and align the data using MapSplice2
  - `novoalign`: contains the scripts to create the reference index and align the data using Novoalign
  - `olego`: contains the scripts to create the reference index and align the data using Olego
  - `rum`: contains the scripts to create the reference index and align the data using RUM
  - `settings`: contains the configuration files containing the tool and data paths
  - `soapsplice`: contains the scripts to create the reference index and align the data using SOAPsplice
  - `star`: contains the scripts to create the reference index and align the data using STAR
  - `subread`: contains the scripts to create the reference index and align the data using Subread
  - `tophat2`: contains the scripts to create the reference index and align the data using TopHat2

#### scripts 
miscellaneous scripts (conversion to/from .BED file format, parsing of .FASTA/.FASTQ and .SAM/.BAM file, adapters simulation, adapters removal, etc.)

---

# Usage

#### Define paths and variables

The folder `job/settings` contains a file for each library (`dataset_`*`<dataset_name>`*`.sh`) and a configuration file (`variable.sh`). The files contained in this folder are designed to work with the data in http://bioinf.itmat.upenn.edu/BEERS/bp1/datasets.php

The files `dataset_<dataset_name>.sh` contain the information about the single libraries: the name of the library, information about the reads (read length, inner distance, etc.), the paths and the file names of genome, reads, annotation and simulator output. If you want to use the default benchmark data, you can employ these files just updating the file paths information (according with your file locations). If you want to use new data, you should create a new file using the previous ones as template.

The file ``variable.sh`` contains the paths and the commands of each software employed in the benchmark. You should change the software paths according with your installation directories.



#### Create a reference index and align the reads

Each folder `job/`*`<aligner_name>`* contains the script to create the reference index and align the data using *<aligner_name>*. For example, the folder `job/hisat2` contains the script used by HISAT2.

Each folder contains the following scripts:  `<aligner_name>-index.sh`, `<aligner_name>-align.sh` and (optional) `<aligner_name>-annotation.sh`. For example, the folder `job/hisat2` contains the scripts `hisat2-index.sh`, `hisat2-align.sh` and `hisat2-annotation.sh`. 

Some tools require to convert the annotation from a standard format (like .GTF or .BED) to a custom format. The script `<aligner_name>-annotation.sh` performs this kind of conversion.  
The script `<aligner_name>-index.sh` creates the reference index while `<aligner_name>-align.sh` performs the read alignment.

For each script `<aligner_name>-annotation.sh`, `<aligner_name>-index.sh` and `<aligner_name>-align.sh` there is a `.job` version compatible with the LSF platform. We suggest to employ the `.job` if you system support the LSF platform.

Each script, both `.sh` and `.job` version, has two input parameters: the first is the number of threads and the second is the dataset file (file `dataset_`*`<dataset_name>`*`.sh`, see the section **"Define paths and variables"**).

For example, employing the HISAT2 scripts to analyze the dataset `dataset_human_hg19_t1r1.sh`, using 16 threads, results in the following commands:
```bash
# create the annotation
hisat2-annotation.sh 16 home/job/settings/dataset_human_hg19_t1r1.sh 
# create the reference index
hisat2-index.sh 16 home/job/settings/dataset_human_hg19_t1r1.sh
# align the reads 
hisat2-align.sh 16 home/job/settings/dataset_human_hg19_t1r1.sh
```

#### Other alignment options

The scripts `<aligner_name>-align-noannotiation.sh` perform the alignment using no annotation for the tool `<aligner_name>`. The scripts have the same input parameters used by `<aligner_name>-align.sh`.

The scripts `<aligner_name>-align-tune.sh` performs the alignment using the mapping options specified as parameters. The number of mapping options depends on the tool, please look the single script to figure out the required parameters.

---

# Software/Hardware requirements
- 64 bit Linux or Mac OS X
- RAM: the amount of RAM depends on the tool and the dataset
- alignment tools correctly installed
- python 3.4.2  

For the complete list of required tools, please see http://bioinf.itmat.upenn.edu/BEERS/bp1/software.php